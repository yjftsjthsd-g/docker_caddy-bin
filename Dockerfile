FROM alpine as build
LABEL maintainer "Brian Cole <docker@brianecole.com>"

# This is terrible. This whole thing is a FIXME.

RUN apk --update --no-cache add wget
WORKDIR /root
RUN wget --no-verbose -O caddy.tar.gz 'https://caddyserver.com/download/linux/amd64?license=personal&telemetry=off'
RUN tar xvzf caddy.tar.gz
RUN ls


# TODO: FROM scratch
FROM alpine AS app
LABEL maintainer "Brian Cole <docker@brianecole.com>"

COPY --from=build /root/caddy /caddy
COPY ./Caddyfile /Caddyfile

WORKDIR /

#ENV IMAKECPP cpp

#USER root
#ENTRYPOINT ["/usr/sbin/sshd", "-D"]

#USER myuser
#ENTRYPOINT ["/opt/start.sh"]

EXPOSE 80 443 2015

ENTRYPOINT ["/caddy"]
CMD ["--conf", "/Caddyfile", "--log", "stdout"]
