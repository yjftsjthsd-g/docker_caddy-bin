# caddy-bin docker image

Downloads Caddy binary from upstream, and sticks it on an Alpine image.
This is terrible; replace ASAP.


## Use

```
sudo docker build . -t caddy-bin
sudo docker run --rm -p 2015:2015 caddy-bin:latest
```
You'll also want to map in volumes, starting with your real Caddyfile.

TODO: docker-compose

